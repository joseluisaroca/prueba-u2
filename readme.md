# Backend Test - PHP

## Instrucciones

* Ingresa al directorio donde se va a instalar el software
Si es Linux, generalmente: `cd /var/www/html`.
Si es Windows, generalmente: `cd C:\xampp\htdocs`.
* Ejecuta `git clone https://gitlab.com/joseleoso/prueba-u2.git`.
* Ingresa a la carpeta del repositorio `cd prueba-u2`.
* Ejecuta `php composer.phar install` o si composer está instalado globalmente `composer install`.
* Ejecuta `cp .env.example .env`.

## Configuración
1. El proyecto se encuentra en Laravel 5.6.
2. Edita el archivo `.env` y establece las configuraciones relevantes de la aplicación.
3. Se crea la base de datos que se encuentra en el archivo `.env` y se hacen las respectivas migraciones usando `php artisan fresh --seed`

***Muchas Gracias!***
