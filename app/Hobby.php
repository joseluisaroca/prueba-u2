<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hobby extends Model{
    protected $primaryKey = 'hob_id';

    protected $fillable = [
        'hob_name'
    ];
}
