<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'usr_username' => 'admin',
        'usr_name' => $faker->name,
        'usr_email' => 'admin@prueba.com',
        'usr_city' =>  $faker->city,
        'password' => bcrypt('123456'),
        'pro_id' => 1,
        'remember_token' => str_random(10),
    ];
});
