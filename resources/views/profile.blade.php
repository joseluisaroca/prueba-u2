@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Editar Mi Usuario') }}</div>

                <div class="card-body">
                    @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                    @endif

                    <form method="POST" action="{{ route('profile_update') }}" aria-label="{{ __('Editar Mi Usuario') }}">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">


                        <div class="form-group row">
                            <label for="usr_name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="usr_name" type="text" value="{{ $user->usr_name }}" class="form-control{{ $errors->has('usr_name') ? ' is-invalid' : '' }}" name="usr_name" value="{{ old('usr_name') }}" required autofocus>

                                @if ($errors->has('usr_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('usr_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="usr_username" class="col-md-4 col-form-label text-md-right">{{ __('Usuario') }}</label>

                            <div class="col-md-6">
                                <input id="usr_username" type="text" value="{{ $user->usr_username }}" class="form-control{{ $errors->has('usr_username') ? ' is-invalid' : '' }}" name="usr_username" value="{{ old('usr_username') }}" required autofocus>

                                @if ($errors->has('usr_username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('usr_username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="usr_email" class="col-md-4 col-form-label text-md-right">{{ __('Correo Electrónico') }}</label>

                            <div class="col-md-6">
                                <input id="usr_email" type="email" value="{{ $user->usr_email }}" class="form-control{{ $errors->has('usr_email') ? ' is-invalid' : '' }}" name="usr_email" value="{{ old('usr_email') }}" required>

                                @if ($errors->has('usr_email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('usr_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="usr_city" class="col-md-4 col-form-label text-md-right">{{ __('Ciudad') }}</label>

                            <div class="col-md-6">
                                <input id="usr_city" type="text" value="{{ $user->usr_city }}" class="form-control{{ $errors->has('usr_city') ? ' is-invalid' : '' }}" name="usr_city" value="{{ old('usr_city') }}" required autofocus>

                                @if ($errors->has('usr_city'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('usr_city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hobbies" class="col-md-4 col-form-label text-md-right">{{ __('Pasatiempos') }}</label>

                            <div class="col-md-6">
                              <input id="hobbies" type="text" value="{{ $user->hobby }}" class="form-control{{ $errors->has('hobbies') ? ' is-invalid' : '' }}" name="hobbies" value="{{ old('hobbies') }}" required autofocus>
                              Agrega las variables con comas (,)
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Editar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
