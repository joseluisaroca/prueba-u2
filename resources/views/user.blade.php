@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                    @endif

                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Usuario</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Ciudad</th>
                          <th scope="col">Tipo de Perfil</th>
                          <th scope="col">Creado</th>
                          <th scope="col">Modificado</th>
                          <th scope="col">Acción</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($users as $user)
                          <tr>
                            <th scope="row">{{ $user->usr_id }} </th>
                            <td> {{ $user->usr_username }}</td>
                            <td> {{ $user->usr_email }}</td>
                            <td> {{ $user->usr_city }}</td>
                            <td> {{ $user->profile->pro_name }}</td>
                            <td> {{ $user->created_at }}</td>
                            <td> {{ $user->updated_at }}</td>
                            <td> <a class="btn btn-primary" href="{{ route('users.edit', $user->usr_id) }}" role="button"><i class="fas fa-edit"></i></a></td>
                          </tr>    
                        @endforeach
                        
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
