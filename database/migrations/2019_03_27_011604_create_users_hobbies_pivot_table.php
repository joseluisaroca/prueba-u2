<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersHobbiesPivotTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users_hobbies', function (Blueprint $table) {
            $table->integer('hob_id')->unsigned()->index();
            $table->foreign('hob_id')->references('hob_id')->on('hobbies')->onDelete('cascade');
            $table->integer('usr_id')->unsigned()->index();
            $table->foreign('usr_id')->references('usr_id')->on('users')->onDelete('cascade');
            $table->primary(['hob_id', 'usr_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users_hobbies');
    }
}
