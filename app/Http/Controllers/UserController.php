<?php

namespace App\Http\Controllers;

use App\User;
use App\Hobby;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $users =  User::all();
        $auth = Auth::user();

        if($auth->profile->pro_name == "administrator"){
            return view('user', compact('users'));
        } else {
            return redirect('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return redirect('users');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        return redirect('users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user){
        $auth = Auth::user();

        if($auth->profile->pro_name == "administrator"){
            $arrHobbies = array();

            foreach ($user->hobbies as $hobby) {
                $arrHobbies[] = ucfirst(mb_strtolower($hobby->hob_name, 'UTF-8'));
                $user->hobby = implode(", ", $arrHobbies);
            }

            return view('edit', compact('user'));
        } else {
            return redirect('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user){
        $auth = Auth::user();

        if($auth->profile->pro_name == "administrator"){
            $user->update($request->all());

            $hobbies = $request->get('hobbies');
            $arrHobbies = explode(',', $hobbies);
            $arrHobbie2 = array();

            foreach ($arrHobbies as $hobby) {
                $qHobby = Hobby::where('hob_name', trim(mb_strtoupper($hobby, 'UTF-8')))->first();
                if(!$qHobby){
                    $insHobby = Hobby::create(['hob_name' => trim(mb_strtoupper($hobby, 'UTF-8'))]);
                    $arrHobbie2[] = $insHobby->hob_id;
                } else {
                    $arrHobbie2[] = $qHobby->hob_id;
                }
            }

            $user->hobbies()->sync($arrHobbie2);

            return redirect('users')->with('status', 'Perfil Actualizado!');
        } else {
            return redirect('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        return redirect('users');
    }
}
